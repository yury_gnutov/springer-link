class SpringerLinkHomePage < SpringerLinkBasePage
  path '/'

  validate :url, %r{\Ahttps?://[^/]+/\z}

  section :categories_primary
  section :search
end
