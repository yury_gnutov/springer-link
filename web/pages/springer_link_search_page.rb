class SpringerLinkSearchPage < SpringerLinkBasePage
  path '/search'

  validate :url, %r{\Ahttps?://[^/]+/search(\?.*)?\z}

  section :search
  section :search_results_header
  section :search_results_list
  section :refine_search
end
