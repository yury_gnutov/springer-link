class SearchResultsListSection < Howitzer::Web::Section
  me :id, 'results-list'

  element :result_record, 'li'

  def search_results
    result_record_elements
  end

  def find_subelement_text(element, locator)
    element.find(locator, wait: 1).text
  rescue Capybara::ElementNotFound
    ''
  end

  def result_content_type(result_record)
    find_subelement_text(result_record, '.content-type')
  end

  def result_title(result_record)
    find_subelement_text(result_record, '.title')
  end

  def result_authors(result_record)
    find_subelement_text(result_record, '.authors')
  end

  def result_year(result_record)
    find_subelement_text(result_record, '.year').gsub(/\D+/, '\1')
  end

  def page_data
    result_record_elements.map do |result|
      {
        title: result_title(result),
        author: result_authors(result),
        year: result_year(result)
      }
    end
  end
end
