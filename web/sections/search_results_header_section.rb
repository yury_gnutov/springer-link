class SearchResultsHeaderSection < Howitzer::Web::Section
  me '#results .header'

  element :number_of_results_label, '.number-of-search-results-and-search-terms'

  def number_of_results
    number_of_results_label_element.text.gsub(/\D+/, '\1')
  end
end
