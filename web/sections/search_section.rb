class SearchSection < Howitzer::Web::Section
  me :id, 'global-search'

  element :query_field,          :id, 'query'
  element :submit_search_button, :id, 'search'

  def fill_form(fields = {})
    query_field_element.set(fields[:query]) if fields[:query]

    self
  end

  def submit_form
    submit_search_button_element.click
  end
end
