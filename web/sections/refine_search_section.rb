class RefineSearchSection < Howitzer::Web::Section
  me :id, 'kb-nav--aside'

  element :filter, :xpath, ->(type, filter) { "//*[@id='#{type}-facet']//*[@class='facet-title'][text()='#{filter}']" }

  def set_filter(type:, name:)
    filter_element(type, name).click
  end
end
