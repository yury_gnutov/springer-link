# SPRINGER assessment README

## Prerequisites
+ ruby v. >= 2.3.1
+ `gem install bundler`
+ chromedriver v. >= 2.28

## How to run
+ `bundle install`
+ `cucumber features/` or `rake features`