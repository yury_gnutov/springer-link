Given(/(?:I |)open '(\w+)' page$/) do |page|
  page.open
end

When(/(?:I |)fill and submit form in '(\w+)' section on opened '(\w+)' page with the following:/) do |section, page, data|
  page
    .given
    .send("#{section}_section")
    .fill_form(data.rows_hash.with_indifferent_access)
    .submit_form
end

When(/(?:I |)set the following filters on '(\w+)' page:/) do |page, filters|
  filters.raw.each do |type, filter|
    page.given.refine_search_section.set_filter(type: type, name: filter)
  end
end

Then(/(?:I |)should be on '(\w+)' page$/) do |page|
  page.given
end

Then(/(?:I |)should see '(\w+)' in '(\w+)' section of '(\w+)' page$/) do |object, section, page|
  actual_data = page.given.send("#{section}_section").page_data
  expected_data = object.instance_values.symbolize_keys

  expect(actual_data).to include(expected_data)
end

Then(/(?:I |)should see '(\w+)' is '(\w+)' in '(\w+)' section of '(\w+)' page$/) do |key, value, section, page|
  actual_data = page.given.send("#{section}_section").send("#{key}")

  expect(actual_data).to eq(value)
end