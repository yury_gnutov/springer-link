Feature: Search
  In order to get a book from Springer Link
  As a user
  I want to find a book on the service

  Scenario: User finds book by name
    Given I open 'springer_link_home' page
    When I fill and submit form in 'search' section on opened 'springer_link_home' page with the following:
      | query | FACTORY_TEST_BOOK[:title] |
    Then I should be on 'springer_link_search' page
    And should see 'FACTORY_TEST_BOOK' in 'search_results_list' section of 'springer_link_search' page

  Scenario: User finds book using filtering
    Given I open 'springer_link_search' page
    When I set the following filters on 'springer_link_search' page:
      | content-type   | Book         |
      | discipline     | Mathematics  |
      | discipline     | Medicine     |
      | sub-discipline | Applications |
    And should see 'FACTORY_TEST_HAND_BOOK' in 'search_results_list' section of 'springer_link_search' page
    And should see 'number_of_results' is '1' in 'search_results_header' section of 'springer_link_search' page

  Scenario: User could not find book
    Given I open 'springer_link_home' page
    When I fill and submit form in 'search' section on opened 'springer_link_home' page with the following:
      | query | FACTORY_TEST_NOT_FOUND_BOOK[:title] |
    Then I should see 'number_of_results' is '0' in 'search_results_header' section of 'springer_link_search' page