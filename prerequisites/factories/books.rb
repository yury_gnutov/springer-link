FactoryGirl.define do
  factory :book do
    author { '' }
    year { '' }
    title { '' }

    trait :iimss2017 do
      title { 'Intelligent Interactive Multimedia Systems and Services 2017' }
      author { 'Giuseppe De Pietro, Luigi Gallo…' }
      year { '2018' }
    end

    trait :hmmi do
      title { 'Handbook of Mathematical Methods in Imaging' }
      author { 'Otmar Scherzer' }
    end

    trait :non_existing do
      title { 'HarryPotterTheMathematicalIllusion' }
    end

    factory :test_book, traits: [:iimss2017]
    factory :test_hand_book, traits: [:hmmi]
    factory :test_not_found_book, traits: [:non_existing]
  end
end
